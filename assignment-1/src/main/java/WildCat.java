public class WildCat {

    // TODO Complete me!
    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        // TODO Complete me!
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        // TODO Complete me!
        return weight / Math.pow((length / 100),2);
    }
}
