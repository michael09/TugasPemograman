import java.util.ArrayList;
import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class
    private static ArrayList<TrainCar> isiKereta = new ArrayList<>();
    private static ArrayList<TrainCar> gerbongGerbong = new ArrayList<>();

    public static void main(String[] args) {
        // TODO Complete me!
        Scanner input = new Scanner(System.in);
        int jumlahKucing = input.nextInt();
        for (int i = 0; i < jumlahKucing; i++) {
            String masukkan = input.next();
            String[] info = masukkan.split(",");
            String nama = info[0];
            String weight = info[1];
            double weightDouble = Double.parseDouble(weight);
            String length = info[2];
            double lengthDouble = Double.parseDouble(length);
            WildCat kucing = new WildCat(nama, weightDouble, lengthDouble);
            TrainCar kereta = new TrainCar(kucing);
            gerbongGerbong.add(kereta);
        }
        pengaturKereta();
    }

    public static void pengaturKereta() {
        double berat = 0;
        for (TrainCar aGerbongGerbong : gerbongGerbong) {
            berat += aGerbongGerbong.computeTotalWeight();
            isiKereta.add(aGerbongGerbong);
            if (berat > THRESHOLD) {
                berangkat();
                berat = 0;
            }
        }
        if (isiKereta.size() != 0) {
            berangkat();
        }
    }

    public static void berangkat() {
        int panjang = isiKereta.size();
        for (int i = panjang - 1; i > 0; i--) {
            isiKereta.get(i).next = isiKereta.get(i - 1);
        }
        TrainCar keretaTerakhir = isiKereta.get(panjang - 1);
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        keretaTerakhir.printCar();
        double averageBmi = keretaTerakhir.computeTotalMassIndex() / panjang;
        System.out.println("Average mass index of all cats: " + averageBmi);
        String bmi;
        if (averageBmi >= 30) {
            bmi = "*obese*";
        } else if (averageBmi >= 25) {
            bmi = "*overweight*";
        } else if (averageBmi >= 18.5) {
            bmi = "*normal*";
        } else {
            bmi = "*underweight*";
        }
        System.out.println("In average, the cats in the train are " + bmi);
        isiKereta.clear();
    }
}
