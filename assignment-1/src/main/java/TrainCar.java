public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!
    private WildCat cat;
    public TrainCar next;

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this(cat,null);
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.next = next;
        this.cat = cat;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
        if (this.next == null) {
            return this.cat.weight + EMPTY_WEIGHT;
        } else {
            return this.cat.weight + EMPTY_WEIGHT + this.next.computeTotalWeight();

        }
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if (this.next == null) {
            return this.cat.computeMassIndex();
        } else {
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // TODO Complete me!
        if (this.next == null) {
            System.out.println("(" + this.cat.name + ")");
        } else {
            System.out.print("(" + this.cat.name + ")--");
            this.next.printCar();
        }
    }
}
