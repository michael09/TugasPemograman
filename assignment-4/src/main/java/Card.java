import javax.swing.ImageIcon;
import javax.swing.JButton;

class Card {
    private final int id;
    private final JButton button = new JButton();
    private boolean matched = false;
    private final ImageIcon pressed;
    private final ImageIcon icon;

    public Card(int id, ImageIcon icon, ImageIcon pressed) {
        this.id = id;
        this.icon = icon;
        this.pressed = pressed;
        this.button.setIcon(icon);
    }

    public JButton getButton() {
        return button;
    }

    public void pressed() {
        button.setIcon(pressed);
    }

    public void release() {
        button.setIcon(icon);
    }

    public int getId() {
        return this.id;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    public boolean getMatched() {
        return this.matched;
    }
}
