import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;

class Board extends JFrame {

    private List<Card> cards;
    private Card c1;
    private Card c2;
    private Timer time;
    private int reset = 0;
    private int tries = 0;
    private JFrame mainFrame;
    private JLabel labelTries;
    private JLabel labelReset;
    private int reveal = 0;

    public Board() {
        int pairs = 18;
        List<Card> cardsList = new ArrayList<>();
        List<Integer> cardVals = new ArrayList<>();

        for (int i = 0; i < pairs; i++) {
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals);
        String path = "assignment-4//src//main//img//";
        String[] hewan = {"cat", "dog", "eagle", "elephant", "giraffe", "hamster",
                "hippo", "lion", "monkey", "mouse", "parrot",
                "peacock", "rhino", "shark", "whale", "zebra",
                "owl", "cheetah"};
        for (int val : cardVals) {
            Card c = new Card(val, resizeIcon(new ImageIcon(path + "logo.png")),
                    resizeIcon(new ImageIcon(path + hewan[val] + ".png")));
            c.getButton().addActionListener(e -> doTurn(c));
            cardsList.add(c);
        }
        this.cards = cardsList;
        //set up the timer
        time = new Timer(750, e -> checkCards());
        time.setRepeats(false);

        mainFrame = new JFrame("Match-Pair Memory Game");
        mainFrame.setLayout(new BorderLayout());
        JPanel panel0 = new JPanel();
        panel0.setLayout(new GridLayout(6, 6));
        for (Card c : cards) {
            panel0.add(c.getButton());
        }
        JPanel panel1 = new JPanel(new GridLayout(2, 2));
        JButton buttonReveal = new JButton("Reveal all(One Time)");
        buttonReveal.addActionListener(e -> allReveal(cards));
        JButton buttonReset = new JButton("Reset");
        panel1.add(buttonReveal);
        panel1.add(buttonReset);
        labelReset = new JLabel("Number of Reset: " + reset);
        labelTries = new JLabel("Number of Tries: " + tries);
        panel1.add(labelTries);
        panel1.add(labelReset);
        buttonReset.addActionListener(e -> reset(cards));
        mainFrame.add(panel0, BorderLayout.CENTER);
        mainFrame.add(panel1, BorderLayout.SOUTH);
        mainFrame.setPreferredSize(new Dimension(800, 800));
        mainFrame.setLocation(500, 250);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    private void reset(List<Card> cards) {
        reset += 1;
        labelReset.setText("Number of Reset: " + reset);
        for (Card c: cards) {
            c.setMatched(false);
            c.release();
            c.getButton().setEnabled(true);
        }
    }

    private void allReveal(List<Card> cards) {
        if (reveal >= 1) {
            JOptionPane.showMessageDialog(this, "Limit reveal exceeded");
            return;
        }
        reveal += 1;
        Timer t1 = new Timer(1500, e -> allHidden(cards));
        t1.setRepeats(false);
        for (Card c: cards) {
            c.pressed();
        }
        t1.start();
    }

    private void allHidden(List<Card> cards) {
        for (Card c: cards) {
            if (!c.getMatched()) {
                c.release();
            }
        }
    }

    private void doTurn(Card selectedCard) {
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.pressed();
        }

        if (c1 != null && c1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.pressed();
            time.start();
        }
    }

    private void checkCards() {
        if (c1.getId() == c2.getId()) { //match condition
            c1.getButton().setEnabled(false); //disables the button
            c2.getButton().setEnabled(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win!\nNumber of "
                        + "tries: " + tries + "\nNumber of Reset: " + reset);
                System.exit(0);
            }
        } else {
            c1.release();
            c2.release();
            tries += 1;
            labelTries.setText("Number of Tries: " + tries);
        }
        c1 = null; //reset c1 and c2
        c2 = null;
    }

    private boolean isGameWon() {
        for (Card c: this.cards) {
            if (!c.getMatched()) {
                return false;
            }
        }
        return true;
    }

    private static ImageIcon resizeIcon(ImageIcon icon) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(130, 120, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }
}