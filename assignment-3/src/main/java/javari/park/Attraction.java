package javari.park;

import java.util.ArrayList;
import java.util.List;

import javari.animal.Animal;

public class Attraction implements SelectedAttraction {
    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<>();

    public Attraction(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public List<Animal> getPerformers() {
        return this.performers;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            this.performers.add(performer);
            return true;
        }
        return false;
    }
}
