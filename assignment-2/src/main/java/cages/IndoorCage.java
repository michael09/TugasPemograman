package cages;

public class IndoorCage {
    private String size;

    public IndoorCage(int length) {
        if (length <= 45) {
            this.size = "A";
        } else if (length >= 60) {
            this.size = "C";
        } else {
            this.size = "B";
        }
    }

    public String getSize() {
        return size;
    }
}
