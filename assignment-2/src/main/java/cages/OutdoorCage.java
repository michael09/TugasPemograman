package cages;

public class OutdoorCage {
    private String size;

    public OutdoorCage(int length) {
        if (length <= 75) {
            this.size = "A";
        } else if (length >= 90) {
            this.size = "C";
        } else {
            this.size = "B";
        }
    }

    public String getSize() {
        return size;
    }
}
