import animals.Animals;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;

import java.util.ArrayList;

public class Arrangement {
    private static final int JUMLAHLEVEL = 3;

    public static ArrayList<ArrayList<Animals>> pengatur(Animals[] binatang) {
        ArrayList<ArrayList<Animals>> hasil = new ArrayList<>();
        ArrayList<Animals> levelSatu = new ArrayList<>();
        ArrayList<Animals> levelDua = new ArrayList<>();
        ArrayList<Animals> levelTiga = new ArrayList<>();
        int panjang = binatang.length;
        if (panjang >= 3) {
            int panjangCage1 = panjang / JUMLAHLEVEL;
            for (int i = 0; i < panjangCage1; i++) {
                levelSatu.add(binatang[i]);
            }
            int sisa = panjang - panjangCage1;
            int panjangCage2 = sisa / (JUMLAHLEVEL - 1);
            for (int x = panjangCage1; x < panjangCage2 + panjangCage1; x++) {
                levelDua.add(binatang[x]);
            }
            for (int y = panjangCage1 + panjangCage2; y < panjang; y++) {
                levelTiga.add(binatang[y]);
            }
        } else if (panjang == 2) {
            levelSatu.add(binatang[0]);
            levelDua.add(binatang[1]);
        } else {
            levelSatu.add(binatang[0]);
        }
        hasil.add(levelSatu);
        hasil.add(levelDua);
        hasil.add(levelTiga);
        String location;
        if (binatang[0] instanceof Lion || binatang[0] instanceof Eagle) {
            location = "outdoor";
        } else {
            location = "indoor";
        }
        System.out.println("location: " + location);
        printOut(hasil);
        return hasil;
    }

    public static void rearrangement(ArrayList<ArrayList<Animals>> listLama) {
        ArrayList<Animals> levelSatuBaru = new ArrayList<>();
        ArrayList<Animals> levelDuaBaru = new ArrayList<>();
        ArrayList<Animals> levelTigaBaru = new ArrayList<>();
        for (int x = listLama.get(0).size() - 1; x >= 0; x--) {
            levelDuaBaru.add(listLama.get(0).get(x));
        }
        for (int x = listLama.get(1).size() - 1; x >= 0; x--) {
            levelTigaBaru.add(listLama.get(1).get(x));
        }
        for (int x = listLama.get(2).size() - 1; x >= 0; x--) {
            levelSatuBaru.add(listLama.get(2).get(x));
        }
        ArrayList<ArrayList<Animals>> listBaru = new ArrayList<>();
        listBaru.add(levelSatuBaru);
        listBaru.add(levelDuaBaru);
        listBaru.add(levelTigaBaru);
        System.out.println("After rearrangement...");
        printOut(listBaru);
    }

    private static void printOut(ArrayList<ArrayList<Animals>> listHewan) {
        String cage = "";
        int length = 0;
        for (int x = 3; x > 0; x--) {
            System.out.print("level " + x + ": ");
            for (Animals hewan : listHewan.get(x - 1)) {
                System.out.print(hewan.getNama());
                length = hewan.getLength();
                if (hewan instanceof Lion) {
                    cage = ((Lion) hewan).getCage().getSize();
                } else if (hewan instanceof Eagle) {
                    cage = ((Eagle) hewan).getCage().getSize();
                } else if (hewan instanceof Parrot) {
                    cage = ((Parrot) hewan).getCage().getSize();
                } else if (hewan instanceof Hamster) {
                    cage = ((Hamster) hewan).getCage().getSize();
                } else if (hewan instanceof Cat) {
                    cage = ((Cat) hewan).getCage().getSize();
                }
                System.out.print(" (" + length + " - " + cage + "), ");
            }
            System.out.println();
        }
        System.out.println();
    }
}