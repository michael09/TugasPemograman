package animals;

import cages.IndoorCage;

public class Hamster extends Animals {
    private static int count = 0;
    private IndoorCage cage;

    public Hamster(String nama, int length, IndoorCage cage) {
        super(nama, length);
        this.cage = cage;
        count += 1;
    }

    public static int getCount() {
        return count;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public void gnawing() {
        this.makeVoice("ngkkrit.. ngkkrrriiit");
    }

    public void wheel() {
        this.makeVoice("trrr…. trrr...");
    }
}
