package animals;

import cages.OutdoorCage;

public class Eagle extends Animals {
    private static int count = 0;
    private OutdoorCage cage;

    public Eagle(String nama, int length,OutdoorCage cage) {
        super(nama, length);
        this.cage = cage;
        count += 1;
    }

    public static int getCount() {
        return count;
    }

    public OutdoorCage getCage() {
        return cage;
    }

    public void orderToFly() {
        this.makeVoice("kwaakk...");
        System.out.println("You hurt!");
    }
}
