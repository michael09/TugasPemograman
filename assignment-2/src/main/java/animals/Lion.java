package animals;

import cages.OutdoorCage;

public class Lion extends Animals {
    private static int count = 0;
    private OutdoorCage cage;

    public Lion(String nama, int length, OutdoorCage cage) {
        super(nama, length);
        this.cage = cage;
        count += 1;
    }

    public static int getCount() {
        return count;
    }

    public OutdoorCage getCage() {
        return cage;
    }

    public void hunting() {
        System.out.println("Lion is hunting..");
        this.makeVoice("err...!");
    }

    public void brush() {
        System.out.println("Clean the lion's mane..");
        this.makeVoice("Hauhhmm!");
    }

    public void disturb() {
        this.makeVoice("HAUHHMM!");
    }
}
