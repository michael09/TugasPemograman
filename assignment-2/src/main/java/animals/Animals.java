package animals;

public class Animals {
    private String nama;
    private int length;

    public Animals(String nama, int length) {
        this.nama = nama;
        this.length = length;
    }

    public String getNama() {
        return nama;
    }

    public int getLength() {
        return length;
    }

    public void makeVoice(String sound) {
        System.out.println(this.nama + " makes a voice: " + sound);
    }
}
