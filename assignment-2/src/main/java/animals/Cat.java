package animals;

import cages.IndoorCage;

import java.util.Random;

public class Cat extends Animals {
    private static int count = 0;
    private IndoorCage cage;
    private String[] sound = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

    public Cat(String nama, int length, IndoorCage cage) {
        super(nama, length);
        this.cage = cage;
        count += 1;
    }

    public static int getCount() {
        return count;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public void brush() {
        System.out.println("Time to clean " + this.getNama() + "'s fur");

        this.makeVoice("Nyaaan...");
    }

    public void cuddle() {
        this.makeVoice(randomVoice());
    }

    public String randomVoice() {
        int idx = new Random().nextInt(sound.length);
        return sound[idx];
    }

}
