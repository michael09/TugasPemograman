package animals;

import cages.IndoorCage;

public class Parrot extends Animals {
    private static int count = 0;
    private IndoorCage cage;

    public Parrot(String nama, int length, IndoorCage cage) {
        super(nama, length);
        this.cage = cage;
        count += 1;
    }

    public static int getCount() {
        return count;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public void orderToFly() {
        System.out.println("Parrot " + this.getNama() + " flies!");
        this.makeVoice("FLYYYY...");
    }

    public void conversation(String word) {
        System.out.println(this.getNama() + "says: " + word.toUpperCase());
    }

    public void alone() {
        System.out.println(this.getNama() + " says: HM?");
    }
}
