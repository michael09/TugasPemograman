import animals.Animals;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;

import cages.IndoorCage;
import cages.OutdoorCage;

import java.util.Scanner;

public class JavariPark {
    private static final int JENISHEWAN = 5;
    private static String[] namaHewan = {"Cat", "Lion","Eagle", "Parrot", "Hamster"};

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        Animals[][] listAnimal = new Animals[JENISHEWAN][];
        for (int i = 0; i < listAnimal.length; i++) {
            System.out.print(namaHewan[i] + ": ");
            int jumlahHewan;
            try {
                jumlahHewan = input.nextInt();
            } catch (Exception e) {
                System.out.println("Input error");
                return;
            }
            if (jumlahHewan > 0) {
                listAnimal[i] = new Animals[jumlahHewan];
                System.out.println("Provide the informations of " + namaHewan[i] + "(s):");
                String inputData = input.next();
                String[] listData = inputData.split(",");
                for (int x = 0; x < listData.length; x++) {
                    String nama;
                    int panjang;
                    try {
                        String[] dataHewan = listData[x].split("\\|");
                        nama = dataHewan[0];
                        panjang = Integer.parseInt(dataHewan[1]);

                        if (i == 0) {
                            listAnimal[i][x] = new Cat(nama, panjang, new IndoorCage(panjang));
                        } else if (i == 1) {
                            listAnimal[i][x] = new Lion(nama, panjang, new OutdoorCage(panjang));
                        } else if (i == 2) {
                            listAnimal[i][x] = new Eagle(nama, panjang, new OutdoorCage(panjang));
                        } else if (i == 3) {
                            listAnimal[i][x] = new Parrot(nama, panjang, new IndoorCage(panjang));
                        } else if (i == 4) {
                            listAnimal[i][x] = new Hamster(nama, panjang, new IndoorCage(panjang));
                        }
                    } catch (Exception e) {
                        System.out.println("Input error");
                        return;
                    }
                }
            }
        }
        System.out.println("Animals have been successfully recorded!");
        System.out.println();
        System.out.println("=============================================");
        System.out.println("Cage arrangement:");
        for (Animals[] hewan: listAnimal) {
            if (hewan != null) {
                Arrangement.rearrangement((Arrangement.pengatur(hewan)));
            }
        }
        System.out.println("NUMBER OF ANIMALS: ");
        System.out.println("cat: " + Cat.getCount());
        System.out.println("lion: " + Lion.getCount());
        System.out.println("parrot: " + Parrot.getCount());
        System.out.println("eagle: " + Eagle.getCount());
        System.out.println("hamster: " + Hamster.getCount());
        System.out.println();
        System.out.println("=============================================");
        while (true) {
            System.out.println("Which animal you want to visit?");
            System.out.println("1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit");
            int pilihan = 0;
            try {
                pilihan = input.nextInt();
            } catch (Exception e) {
                System.out.println("Input Error");
                break;
            }
            if (pilihan == 99) {
                System.out.println("Bye Bye");
                break;
            }
            String nama = null;
            int index = -1;
            int jumlah = 0;
            switch (pilihan) {
                case 1:
                    nama = "cat";
                    index = 0;
                    jumlah = Cat.getCount();
                    break;
                case 2:
                    nama = "eagle";
                    index = 2;
                    jumlah = Eagle.getCount();
                    break;
                case 3:
                    nama = "hamster";
                    index = 4;
                    jumlah = Hamster.getCount();
                    break;
                case 4:
                    nama = "parrot";
                    index = 3;
                    jumlah = Parrot.getCount();
                    break;
                case 5:
                    nama = "lion";
                    index = 1;
                    jumlah = Lion.getCount();
                    break;
                default:
                    System.out.println("Wrong Command");
                    break;
            }
            if (jumlah > 0) {
                System.out.print("Mention the name of " + nama + " you want to visit: ");
                String namaHewan = input.next();
                boolean flag = false;
                Animals terpilih = null;
                for (Animals hewan: listAnimal[index]) {
                    if (hewan.getNama().equals(namaHewan)) {
                        flag = true;
                        terpilih = hewan;
                        break;
                    }
                }
                if (flag) {
                    System.out.println("You are visiting "
                            + terpilih.getNama() + " (" + nama + ") now, "
                            + "what would you like to do?");
                    switch (index) {
                        case 0:
                            System.out.println("1: brush the fur 2: cuddle");
                            int choose = input.nextInt();
                            switch (choose) {
                                case 1: ((Cat)terpilih).brush();
                                break;
                                case 2: ((Cat)terpilih).cuddle();
                                break;
                                default: System.out.println("You do nothing...");
                                break;
                            }
                            break;
                        case 2:
                            System.out.println("1: Order to fly");
                            int choose1 = input.nextInt();
                            switch (choose1) {
                                case 1: ((Eagle)terpilih).orderToFly();
                                break;
                                default: System.out.println("You do nothing...");
                                break;
                            }
                            break;
                        case 4:
                            System.out.println("1: See it gnawing 2: Order to run in the ha"
                                    + "mster wheel");
                            int choose2 = input.nextInt();
                            switch (choose2) {
                                case 1: ((Hamster)terpilih).gnawing();
                                break;
                                case 2: ((Hamster)terpilih).wheel();
                                break;
                                default: System.out.println("You do nothing");
                                break;
                            }
                            break;
                        case 3:
                            System.out.println("1: Order to fly 2: Do conversation");
                            int choose3 = input.nextInt();
                            switch (choose3) {
                                case 1: ((Parrot)terpilih).orderToFly();
                                break;
                                case 2:
                                    System.out.print("You say: ");
                                    input.nextLine();
                                    String word = input.nextLine();
                                    ((Parrot)terpilih).conversation(word);
                                    break;
                                default: ((Parrot)terpilih).alone();
                                break;
                            }
                            break;
                        case 1:
                            System.out.println("1: See it hunting 2: brush the mane 3: disturb it");
                            int choose4 = input.nextInt();
                            switch (choose4) {
                                case 1: ((Lion)terpilih).hunting();
                                break;
                                case 2: ((Lion)terpilih).brush();
                                break;
                                case 3: ((Lion)terpilih).disturb();
                                break;
                                default: System.out.println("You do nothing");
                                break;
                            }
                            break;
                        default: System.out.println("You choose nothing");
                        break;
                    }
                    System.out.println("Back to office!");
                } else {
                    System.out.println("There is no " + nama + " with that name! Back to office!");
                }
            } else if (pilihan < 0 || pilihan > JENISHEWAN) {
                System.out.println("Back to Office");
            } else {
                System.out.println("There are no " + nama + " in this park");
            }
        }
    }
}
